import React from "react";
import { Col, Row } from "react-bootstrap";
import LoginCard from "../components/LoginCard";
import RegisterCard from "../components/RegisterCard";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: false,
    };
    this.togglebutton = this.togglebutton.bind(this);
  }
  togglebutton() {
    this.setState({
      isShow: !this.state.isShow,
    });
    console.log("testing ");
  }

  render() {
    const isShow = this.state.isShow;
    let buttonText;
    if (isShow) {
      buttonText = "Login";
    } else {
      buttonText = "Register";
    }

    return (
      <div style={{ marginTop: "150px" }}>
        <Row>
          <Col md={{ span: 6, offset: 3 }}>
            <Row>
              <Col md={{ span: 6, offset: 3 }}>
                {isShow ? (
                  <div>
                    <RegisterCard />
                  </div>
                ) : (
                  <div>
                    <LoginCard />
                  </div>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
        <span onClick={this.togglebutton}>Click here to: {buttonText}</span>
      </div>
    );
  }
}
export default LoginPage;
