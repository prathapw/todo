import React from "react";
import NavBar from "../components/Navbar";
import CourseTable from "../components/Table";
import { Button, Form, Card } from "react-bootstrap";

class HomePage extends React.Component {
  render() {
    const addCard = (
      <Card style={{ width: "18rem", marginTop: "50px", marginLeft:"35%" }}>
        <Card.Body>
          <Card.Title>Add Course</Card.Title>
          <Form>
            <Form.Group className="mb-3" controlId="formBasicCourseId">
              <Form.Label>Course ID</Form.Label>
              <Form.Control type="email" placeholder="Enter courseID" />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicCourseName">
              <Form.Label>Course Name</Form.Label>
              <Form.Control type="text" placeholder="Enter courseName" />
            </Form.Group>
            <Button variant="primary" type="submit">
              ADD
            </Button>
          </Form>
        </Card.Body>
      </Card>
    );
    return (
      <div className="homePage">
        <NavBar />
        {addCard}
        <CourseTable />
      </div>
    );
  }
}
export default HomePage;
