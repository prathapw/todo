import React from "react";
import { Table, Button } from "react-bootstrap";

class CourseTable extends React.Component {
  render() {
    return (
      <div className="table" style={{ marginTop: "20px" }}>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Course ID</th>
              <th>Course Name</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>Course1</td>
              <td>
                <Button variant="dark" size="sm">Delete</Button>
              </td>
            </tr>
            <tr>
              <td>2</td>
              <td>Course2</td>
              <td>
                <Button variant="dark" size="sm">Delete</Button>
              </td>
            </tr>
            <tr>
              <td>3</td>
              <td>Course3</td>
              <td>
                <Button variant="dark" size="sm">Delete</Button>
              </td>
            </tr>
          </tbody>
        </Table>
      </div>
    );
  }
}
export default CourseTable;
