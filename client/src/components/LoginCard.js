import React from "react";
import { Button, Form, Card } from "react-bootstrap";

class LoginCard extends React.Component {
  render() {
    return (
      <div className="loginCard">
        <Card style={{ width: "18rem" }}>
          <Card.Body>
            <Card.Title>Sign In</Card.Title>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>User Name</Form.Label>
                <Form.Control type="email" placeholder="Enter userName" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>
              <Button variant="primary" type="submit">
                Login
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
export default LoginCard;
