import React from "react";
import { Button, Form, Card } from "react-bootstrap";

class RegisterCard extends React.Component {
  render() {
    return (
      <div className="registerCard">
        <Card style={{ width: "18rem" }}>
          <Card.Body>
            <Card.Title>Sign Up</Card.Title>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>User Name</Form.Label>
                <Form.Control type="email" placeholder="Enter userName" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>
              <Button variant="primary" type="submit">
                Submit
              </Button>
            </Form>
          </Card.Body>
        </Card>
      </div>
    );
  }
}
export default RegisterCard;
