const express = require('express');
const router = express.Router();
const { createUser, userLogin} = require("../../controllers/userController")
//create user
router.post( "/createUser", createUser );

//login checking
router.post( "/login", userLogin );

module.exports = router;